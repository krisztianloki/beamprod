"""
Generator script for EPICS db template file contating mbbo/mbbi record types.

Arek Gorzawski 2021, ESS
"""

from jinja2 import Environment, FileSystemLoader
import datetime
import yaml
import socket
import getpass
import re
import requests

excluded = (None, "None", "ERROR", "UNDEFINED")
template_file_name = 'beamprod{}-mode-dest.template.jinja'

input_yaml = r'databuffer-ess.yml'
urlToLatest = 'https://gitlab.esss.lu.se/icshwi/reftabs/-/raw/master/init/{}?inline=false'.format(input_yaml)
print('Downloading the latest {}'.format(input_yaml))
print('Source: {}'.format(urlToLatest))
r = requests.get(urlToLatest, allow_redirects=True)
open(input_yaml, 'wb').write(r.content)

result = {'BDest':[],
          'BMod':[],
          'BState':[],
          'BPresent':[],
         }

class Element:
    # TODO Expand this (EPICS supports up to 16) when needed
    epics_mbbo_value = ["ZRVL", "ONVL", "TWVL", "THVL", "FRVL", "FVVL", "SXVL", "SVVL",
                        "EIVL", "NIVL", "TEVL", "ELVL", "TVVL"]
    epics_mbbo_string= ["ZRST", "ONST", "TWST", "THST", "FRST", "FVST", "SXST", "SVST",
                        "EIST", "NIST", "TEST", "ELST", "TVST"]
    def __init__(self, val, epics_order, label, description):
        self.val = val
        self.epics_value = self.epics_mbbo_value[epics_order]
        self.epics_string = self.epics_mbbo_string[epics_order]
        self.label = label
        self.description = description


def split_label(label):
    splitCC = re.sub('([A-Z][a-z]+)', r' \1', re.sub('([A-Z]+)', r' \1', label)).split()
    strOut = ""
    for one in splitCC:
        strOut += one + " "
    return strOut.rstrip()

def correct_label(label):
    ''' yaml reads On/Off as booleans '''
    if label is True:
        return 'On'
    elif label is False:
        return 'Off'
    else:
        return label

with open(input_yaml) as file:
    for item, doc in yaml.full_load(file).items():
        if item in list(result.keys()):
            for indx, one in enumerate(doc.keys()):
                if one in excluded:
                    continue
                result[item].append(Element(val=doc[one]['id'],
                                            #val=correct_label(one),
                                            epics_order=indx,
                                            label=split_label(correct_label(one)),
                                            description=doc[one].get('description','none')))

file_loader = FileSystemLoader('.')
env = Environment(loader=file_loader)
template = env.get_template(template_file_name.format(""))

# BeamProd DB file
output = template.render(PBMode='$(P):$(R):BMode',
                         PBDest='$(P):$(R):BState',
                         PBState='$(P):$(R):BDestination',
                         beammodes=result['BMod'],
                         beamdestinations=result['BDest'],
                         beamstate=result['BState'],
                         beampresent=result['BPresent'],
                         generated_date=datetime.datetime.now(),
                         username=getpass.getuser(),
                         computer=socket.gethostname(),
                         forward=True)
with open(template_file_name.format("").replace('.jinja',''), "w") as fh:
    fh.write(output)

# SCE DB TEST file
output = template.render(PBMode='$(PP):BMod-Sel',
                         PBDest='$(PP):BState-Sel',
                         PBState='$(PP):BDest-Sel',
                         beammodes=result['BMod'],
                         beamdestinations=result['BDest'],
                         beamstate=result['BState'],
                         beampresent=result['BPresent'],
                         generated_date=datetime.datetime.now(),
                         username=getpass.getuser(),
                         computer=socket.gethostname(),
                         forward=False)
with open(template_file_name.format("-TEST").replace('.jinja',''), "w") as fh:
    fh.write(output)
