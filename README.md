# e3-beamprod: Operations Beam Production EPICS Module

This IOC acts as a proxy between the dedicated OPI and the SCE IOC.
The following PVs are exposed:

- {BPROD}:PBMode, internally forward link to {SCE}:PBMod-Sel
- {BPROD}:PBState, internally forward link to {SCE}:PBState-Sel
- *{BPROD}:PBPresent* unused
- {BPROD}:PBDestination, internally forward link to {SCE}:PBDest-Sel

- *{BPROD}:TTsAvailable* unused
- *{BPROD}:TTRunning* unused
- *{BPROD}:TTAdd* unused
- {BPROD}:TTLoaded, forward to {SCE}:ScTable-SP
- {BPROD}:TTSource, forward to {SCE}:ScTableDir-SP

## Code generation after definitions update

Part of the PVs in this IOC (:PBState, :PBMode and :PBDestination) are automatically generated via python code + jinja template (all included). This is the case for this IOC and for the TestEnviroment (see below).
EPICS DB file related to these PVs is part of the committed repository.

## Test Environment

To work with this IOC, one may enable simulated SCE IOC with the PVs exposed and used as in production.
After starting the IOC (with proper TEST lines enabled in `cmds/beamprod.cmd`) one should initiate the waveform PVs

```bash
pvput TD-M:Ctrl-SCE-1:ScTables-I as X_A_asdas X_B_asda X_C_asd anotherExample
```
