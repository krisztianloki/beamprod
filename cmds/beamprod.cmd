
epicsEnvSet("TOP","$(E3_CMD_TOP)/..")
epicsEnvSet("APP", "$(TOP)/beamprod/beamprodApp")

dbLoadRecords("$(APP)/Db/beamprod-timing.template","P=BPROD, R=Ops, PP=TD-M:Ctrl-SCE-1")
dbLoadRecords("$(APP)/Db/beamprod-mode-dest.template","P=BPROD, R=Ops, PP=TD-M:Ctrl-SCE-1")

# TODO is the any env/runtime way to disable that load?
# ONE MUST maintain this disabled in production!
# the following DB mimics the exteral IOC that BeamProd is talking to.
#dbLoadRecords("$(APP)/Db/beamprod-TEST.template","P=BPROD, R=Ops, PP=TD-M:Ctrl-SCE-1")
#dbLoadRecords("$(APP)/Db/beamprod-TEST-mode-dest.template","P=BPROD, R=Ops, PP=TD-M:Ctrl-SCE-1")

# always leave a blank line at EOF
